# this is a test ...
#FROM node:13.12-slim
FROM registry.gitlab.com/tanuki-workshops/kube-demos/demo-metrics/node:13.12-slim
COPY . .
RUN npm install
CMD [ "node", "index.js" ]
